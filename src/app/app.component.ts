import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'chicago311UI';

  constructor(private authService: AuthenticationService) {}

  ngOnInit(): void {
  }

}
