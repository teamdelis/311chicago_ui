// Generated using typescript-generator version 1.29.366 on 2018-12-09 16:58:21.

export class AbandonedVehicleDetails {
  requestId: number;
  licensePlate: string;
  numberOfDatsParked: number;
  vehicleColor: string;
  vehicleMakeModel: string;
}

export class Actions {
  action: string;
}

export class ActionsPerType {
  requestType: string;
  action: string;
}

export class ActionsPerTypePK {
  requestType: string;
  action: string;
}

export class Activities {
  activity: string;
}

export class ActivitiesPerType {
  requestType: string;
  activity: string;
}

export class ActivitiesPerTypePK {
  requestType: string;
  activity: string;
}

export class GarbageCartsDelivered {
  requestId: number;
  numberOfCarts: number;
}

export class GraffitiDetails {
  requestId: number;
  structureType: string;
  surfaceType: string;
}

export class LocationOfTreeInRequest {
  requestId: number;
  treeLocation: string;
}

export class Logs {
  id: number;
  event: string;
  timestamp: Timestamp;
}

export class PotholesFilled {
  requestId: number;
  numberOfFilledPotholes: number;
}

export class RecentActionsOnRequest {
  requestId: number;
  currentActivity: string;
  mostRecentAction: string;
}

export class Request {
  id: number;
  communityArea: number;
  completionDate: string;
  creationDate: string;
  latitude: number;
  longitude: number;
  policeDistrict: number;
  serviceRequestNumber: string;
  status: string;
  streetNumber: string;
  ward: number;
  xCoordinate: number;
  yCoordinate: number;
  abandonedVehicleDetails: AbandonedVehicleDetails;
  garbageCartsDelivered: GarbageCartsDelivered;
  graffitiDetails: GraffitiDetails;
  locationOfTreeInRequest: LocationOfTreeInRequest;
  potholesFilled: PotholesFilled;
  recentActionsOnRequest: RecentActionsOnRequest;
  requestType: RequestType;
  streetName: StreetNames;
  zipCode: ZipCodes;
  rodentBaitingDetails: RodentBaitingDetails;
  sanitationViolationsInRequest: SanitationViolationsInRequest;
  ssa: SsaInRequest;
}

export class RequestType {
  type: string;
}

export class RodentBaitingDetails {
  requestId: number;
  numberOfPremisesBaited: number;
  numberOfPremisesWithGarbage: number;
  numberOfPremisesWithRats: number;
}

export class SanitationCodeViolations {
  codeViolation: string;
}

export class SanitationViolationsInRequest {
  requestId: number;
  violation: string;
}

export class SsaInRequest {
  requestId: number;
  ssa: number;
}

export class StoredFunctions {
  c1: string;
  id: string;
}

export class StreetNames {
  streetName: string;
}

export class StructuresWithGraffiti {
  structure: string;
}

export class SurfacesWithGraffiti {
  surface: string;
}

export class User {
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  street: string;
  streetno: string;
  logs: Logs[];
}

export class VehicleColor {
  color: string;
}

export class VehicleMakeModel {
  makeModel: string;
}

export class ZipCodes {
  zipCode: number;
}


export class DateT extends Date {
}

export class Timestamp extends DateT {}
