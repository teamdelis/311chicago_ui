import { Validators } from '@angular/forms';

export const requestBaseDefinition = {
  id: [''],
  creationDate: [''],
  status: ['', Validators.required],
  completionDate: [''],
  serviceRequestNumber: [''],
  ward: ['', [Validators.min(1), Validators.max(50)]],
  policeDistrict: ['', [Validators.min(1), Validators.max(25)]],
  communityArea: ['', [Validators.min(1), Validators.max(77)]],
  streetName: ['', Validators.required],
  streetNumber: ['', Validators.required],
  zipCode: ['', Validators.required],
  xCoordinate: [''],
  yCoordinate: [''],
  latitude: [''],
  longitude: ['']
};

export const ssaDefinition = {
  requestId: [''],
  ssa: ['', [Validators.min(1), Validators.max(73)]]
};

export const recentActionsDefinition = {
  requestId: [''],
  currentActivity: [''],
  mostRecentAction: ['']
};

export const garbageCartsDeliveredDefinition = {
  requestId: [''],
  numberOfCarts: ['']
};

export const potholesFilledDefinition = {
  requestId: [''],
  numberOfFilledPotholes: ['']
};

export const treeLocDefinition = {
  requestId: [''],
  treeLocation: ['']
};

export const sanitationViolationsDefinition = {
  requestId: [''],
  violation: ['']
};

export const rodentBaitingDefinition = {
  requestId: [''],
  numberOfPremisesBaited: [''],
  numberOfPremisesWithGarbage: [''],
  numberOfPremisesWithRats: ['']
};

export const graffitiDefinition = {
  requestId: [''],
  surfaceType: [''],
  structureType: ['']
};

export const abandonedVehicleDefinition = {
  requestId: [''],
  licensePlate: [''],
  vehicleMakeModel: [''],
  vehicleColor: [''],
  numberOfDatsParked: ['']
};
