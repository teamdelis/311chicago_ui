export const requestTypesMap = {
  abandoned_vehicle: 'Abandoned Vehicle Complaint',
  alley_lights: 'Alley Light Out',
  garbage_cart: 'Garbage Cart Black Maintenance/Replacement',
  graffiti: 'Graffiti Removal',
  pothole: 'Pot Hole in Street',
  rodents: 'Rodent Baiting/Rat Complaint',
  sanitation: 'Sanitation Code Violation',
  tree_debris: 'Tree Debris',
  tree_trim: 'Tree Trim',
  all_street_lights: 'Street Lights - All/Out',
  one_street_light: 'Street Light - 1/Out'
};

