import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

const headerOptions = {
  headers : new HttpHeaders().set('Content-Type', 'application/json')
    .set('Accept', 'application/json'),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class StoredProceduresService {
  apiUrl = environment.API_ENDPOINT + '/request/';

  constructor(private http: HttpClient) { }

  getTotalRequestsPerTypeInPeriod(fromDate: string, toDate: string, includeDuplicates: boolean): Observable<any[]> {
    // const url = `${this.apiUrl}getTotalPerTypeInPeriod/${fromDate}/${toDate}/${includeDuplicates}`;
    const url = `${this.apiUrl}query1?from=${fromDate}&to=${toDate}&duplicates=${includeDuplicates}`;
    return this.http.get<any[]>(url, headerOptions);
  }

  getTotalRequestsPerDayInPeriod(fromDate: string, toDate: string, reqType: string, includeDuplicates: boolean): Observable<any> {
    // let url = `${this.apiUrl}getTotalPerDayInPeriod/${fromDate}/${toDate}`;
    let url = `${this.apiUrl}query2?from=${fromDate}&to=${toDate}`;
    url = `${url}&type=${encodeURIComponent(reqType)}&duplicates=${includeDuplicates}`;
    return this.http.get<any>(url, headerOptions);
  }

  getPopularRequestTypePerZipcodeInDay(day: string, includeDuplicates: boolean): Observable<any[]> {
    // const url = `${this.apiUrl}getMostCommonReqTypeInDay/${day}/${includeDuplicates}`;
    const url = `${this.apiUrl}query3?date=${day}&duplicates=${includeDuplicates}`;
    return this.http.get<any[]>(url, headerOptions);
  }
}
