import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import {
  Request, SanitationCodeViolations,
  StreetNames, StructuresWithGraffiti, SurfacesWithGraffiti,
  VehicleColor,
  VehicleMakeModel,
  ZipCodes
} from '../domain/typeScriptClasses';


const headerOptions = {
  headers : new HttpHeaders().set('Content-Type', 'application/json')
    .set('Accept', 'application/json'),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {
  apiUrl = environment.API_ENDPOINT;

  constructor(private http: HttpClient) { }

  /*requests*/
  addRequest(newRequest: Request): Observable<Request> {
    const url = this.apiUrl + '/request/add';
    return this.http.post<Request>(url, JSON.stringify(newRequest), headerOptions);
  }

  updateRequest(updatedRequest: Request): Observable<Request> {
    const url = this.apiUrl + '/request/update';
    return this.http.post<Request>(url, JSON.stringify(updatedRequest), headerOptions);
  }

  getRequestById(requestId: string): Observable<Request> {
    const url = this.apiUrl + '/request/getByID/' + requestId;
    return this.http.get<Request>(url, headerOptions);
  }

  getRequestsInStreet(strName: string, offset: number, quantity: number): Observable<any> {
    const url = `${this.apiUrl}/request/getByStreetName?name=${encodeURIComponent(strName)}&from=${offset}&quantity=${quantity}`;
    return this.http.get<any>(url, headerOptions);
  }

  getRequestsInZipcode(zipCode: string, offset: number, quantity: number): Observable<any> {
    const url = `${this.apiUrl}/request/getByZipCode?code=${zipCode}&from=${offset}&quantity=${quantity}`;
    return this.http.get<any>(url, headerOptions);
  }


  /*other data*/
  getAllStreetNames(): Observable<StreetNames[]> {
    const url = this.apiUrl + '/streetName/getAll';
    return this.http.get<StreetNames[]>(url, headerOptions);
  }

  getAllZipCodes(): Observable<ZipCodes[]> {
    const url = this.apiUrl + '/zipCode/getAll';
    return this.http.get<ZipCodes[]>(url, headerOptions);
  }

  getActivitiesByType(requestType: string): Observable<string[]> {
    const url = this.apiUrl + '/activities?type=' + encodeURIComponent(requestType);
    return this.http.get<string[]>(url, headerOptions);
  }

  getActionsByType(requestType: string): Observable<string[]> {
    const url = this.apiUrl + '/actions?type=' + encodeURIComponent(requestType);
    return this.http.get<string[]>(url, headerOptions);
  }

  getAllVehicleMakeModels(): Observable<VehicleMakeModel[]> {
    const url = this.apiUrl + '/vehicleMakeModels/getAll';
    return this.http.get<VehicleMakeModel[]>(url, headerOptions);
  }

  getAllVehicleColors(): Observable<VehicleColor[]> {
    const url = this.apiUrl + '/vehicleColors/getAll';
    return this.http.get<VehicleColor[]>(url, headerOptions);
  }

  getAllGraffitiSurfaces(): Observable<SurfacesWithGraffiti[]> {
    const url = this.apiUrl + '/graffitiSurfaces/getAll';
    return this.http.get<SurfacesWithGraffiti[]>(url, headerOptions);
  }

  getAllGraffitiStructures(): Observable<StructuresWithGraffiti[]> {
    const url = this.apiUrl + '/graffitiStructures/getAll';
    return this.http.get<StructuresWithGraffiti[]>(url, headerOptions);
  }

  getAllSanitationViolations(): Observable<SanitationCodeViolations[]> {
    const url = this.apiUrl + '/sanitationCodeViolations/getAll';
    return this.http.get<SanitationCodeViolations[]>(url, headerOptions);
  }

}
