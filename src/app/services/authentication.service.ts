import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { deleteCookie, getCookie } from '../utils/cookie-funs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../domain/typeScriptClasses';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

const headerOptions = {
  headers : new HttpHeaders().set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  redirectUrl = '/home';
  cookieName = 'currentUser';
  apiUrl = environment.API_ENDPOINT + '/user/';


  constructor(private http: HttpClient, private router: Router) { }


  isLoggedIn(): boolean {
    return (getCookie(this.cookieName) !== null) && (this.getUserName() !== null);
  }

  /*getUserProperty(property: string) {
    const user = sessionStorage.getItem('userInfo');
    if ( user && user[property] && (user[property] !== 'null') ) {
      return user[property];
    }
    return null;
  }*/

  getUserName() {
    const username = sessionStorage.getItem('username');
    if ( username && (username !== 'null') ) {
      return username;
    }
    return null;
  }

  login(username: string, password: string) {
    if (getCookie(this.cookieName) !== null) {
      console.log('found cookie');
      sessionStorage.setItem('username', username);
      window.location.href = '/home';
    } else {
      const url = this.apiUrl + 'login';
      sessionStorage.setItem('username', username);

      const headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Accept', 'application/json').set('username', username).set('password', password);

      this.http.post(url, {username: username}, {headers}).subscribe(
        us => {
          console.log('after login, cookie is' , getCookie(this.cookieName));
        },
        er => {
          console.log(er);
          sessionStorage.clear();
        },
        () => {
          if (!getCookie(this.cookieName)) {
            this.logout();
          }
          this.router.navigate(['home']);
        }
      );
    }
  }

  register(newUser: User): Observable<any> {
    console.log('registering user');
    sessionStorage.setItem('username', newUser.username);
    return this.http.post<any>(this.apiUrl + 'register', newUser, headerOptions);
  }

  logout() {
    deleteCookie(this.cookieName);
    sessionStorage.clear();
    this.http.get(this.apiUrl + 'logout', headerOptions).subscribe(
      us => console.log(JSON.stringify(us)),
      er => console.log(er),
      () => this.router.navigate(['home'])
    );
  }

  getAllUsernames(): Observable<string[]> {
    return this.http.get<string[]>(this.apiUrl + 'getAllUsernames', headerOptions);
  }

}
