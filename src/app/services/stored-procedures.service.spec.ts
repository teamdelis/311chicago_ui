import { TestBed } from '@angular/core/testing';

import { StoredProceduresService } from './stored-procedures.service';

describe('StoredProceduresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoredProceduresService = TestBed.get(StoredProceduresService);
    expect(service).toBeTruthy();
  });
});
