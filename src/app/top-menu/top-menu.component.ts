import { Component, OnInit } from '@angular/core';
import { requestTypesMap } from '../domain/extra-classes';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html'
})
export class TopMenuComponent implements OnInit {

  requestTypes = requestTypesMap;
  typesArray: string[] = [];

  constructor(private router: Router,
              private authService: AuthenticationService) { }

  ngOnInit() {
    this.typesArray = Object.keys(this.requestTypes);
  }

  goToNewRequest(type: string) {
    window.location.href = `/requests/new/${type}`;
  }

  isUserLoggedIn() {
    return this.authService.isLoggedIn();
  }

  getUsername() {
    return this.authService.getUserName();
  }

  login() {
    if (!this.isUserLoggedIn()) {
      this.router.navigate(['home']);
    }
  }


  logout() {
    this.authService.logout();
  }

}
