import { Component, OnInit } from '@angular/core';
import { StoredProceduresService } from '../services/stored-procedures.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { requestTypesMap } from '../domain/extra-classes';

@Component({
  selector: 'app-requests-stats',
  templateUrl: './requests-stats.component.html'
})
export class RequestsStatsComponent implements OnInit {
  queryChoice = 0;
  requestTypes = requestTypesMap;
  typesArray: string[] = [];
  results: any[] = [];
  resultsTitle = 'Query results:';
  searchForm: FormGroup;
  readonly formDefinition = { fromDate: ['', Validators.required],
    toDate: [''],
    reqType: [''],
    includeDup: [true] };

  constructor(private fb: FormBuilder,
              private storedProcService: StoredProceduresService) { }

  ngOnInit() {
    this.typesArray = Object.keys(this.requestTypes);
    this.searchForm = this.fb.group(this.formDefinition, {validator: checkDates});
    console.log('tried to init');
  }

  getTotalReqPerType() {
    if (this.searchForm && this.searchForm.valid && (this.searchForm.get('toDate').value !== '')) {
      this.results = null;
      this.queryChoice = 0;
      this.storedProcService.getTotalRequestsPerTypeInPeriod(
                  this.searchForm.get('fromDate').value,
                  this.searchForm.get('toDate').value,
                  this.searchForm.get('includeDup').value).subscribe(
      res => this.results = res,
      er => console.log(er),
      () => this.queryChoice = 1
      );
    }
  }

  getTotalReqPerDay() {
    if (this.searchForm && this.searchForm.valid &&
      (this.searchForm.get('toDate').value !== '') &&
      (this.searchForm.get('reqType').value !== '')) {

      this.results = null;
      this.queryChoice = 0;
      this.storedProcService.getTotalRequestsPerDayInPeriod(
        this.searchForm.get('fromDate').value,
        this.searchForm.get('toDate').value,
        this.searchForm.get('reqType').value,
        this.searchForm.get('includeDup').value).subscribe(
        res => this.results = res,
        er => console.log(er),
        () => this.queryChoice = 2
      );
    }
  }

  getTotalReqPerZipCodeInDay() {
    if (this.searchForm && this.searchForm.valid) {
      this.results = null;
      this.queryChoice = 0;
      this.storedProcService.getPopularRequestTypePerZipcodeInDay(
                      this.searchForm.get('fromDate').value,
                      this.searchForm.get('includeDup').value).subscribe(
        res => this.results = res,
        er => console.log(er),
        () => this.queryChoice = 3
      );
    }
  }

}

export function checkDates(group: FormGroup) {
  if (!group.get('toDate').value) {
    return null;
  }
  return group.get('fromDate').value <= group.get('toDate').value ? null : {res: true};
}
