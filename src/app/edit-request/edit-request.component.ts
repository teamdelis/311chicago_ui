import { AfterViewInit, Component, ElementRef, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe, DOCUMENT } from '@angular/common';
import {
  abandonedVehicleDefinition,
  garbageCartsDeliveredDefinition, graffitiDefinition,
  potholesFilledDefinition,
  recentActionsDefinition,
  requestBaseDefinition, rodentBaitingDefinition, sanitationViolationsDefinition,
  ssaDefinition, treeLocDefinition
} from '../domain/form-definitions';
import { ActivatedRoute } from '@angular/router';
import { requestTypesMap } from '../domain/extra-classes';
import { ResourcesService } from '../services/resources.service';
import {
  AbandonedVehicleDetails,
  GarbageCartsDelivered, GraffitiDetails, LocationOfTreeInRequest, PotholesFilled,
  RecentActionsOnRequest,
  Request, RodentBaitingDetails,
  SanitationCodeViolations, SanitationViolationsInRequest, SsaInRequest,
  StreetNames, StructuresWithGraffiti,
  SurfacesWithGraffiti, VehicleColor, VehicleMakeModel,
  ZipCodes
} from '../domain/typeScriptClasses';
import { mergeMap, tap } from 'rxjs/operators';

/*
* Source code for OSM rendering and transforming:
* https://medium.com/@balramchavan/using-openstreetmap-inside-angular-v6-3d42cbf03e57
* Source code for passing external script to secondary component (not app.component) [see comment No6]:
* https://stackoverflow.com/questions/38088996/adding-script-tags-in-angular-component-template/43559644
*/
declare var ol: any;

@Component({
  selector: 'app-edit-request',
  templateUrl: './edit-request.component.html'
})
export class EditRequestComponent implements OnInit, AfterViewInit {
  errorMessage: string;
  successMessage: string;

  newRequest: boolean;
  requestType: string;
  requestId: string;
  currentRequest: Request;
  requestTypes = requestTypesMap;
  streetNames: StreetNames[] = [];
  zipCodes: ZipCodes[] = [];

  lat = 41.8336474;
  lon = -87.8723897;
  map: any;

  requestForm: FormGroup;
  readonly requestBaseDef = requestBaseDefinition;
  ssaForm: FormGroup;
  readonly ssaDef = ssaDefinition;
  actionsForm: FormGroup;
  readonly actionsDef = recentActionsDefinition;
  currentActivitiesList: string[] = [];
  mostRecentActionsList: string[] = [];
  garbageForm: FormGroup;
  readonly garbageDef = garbageCartsDeliveredDefinition;
  potholesForm: FormGroup;
  readonly potholesdDef = potholesFilledDefinition;
  treeLocForm: FormGroup;
  readonly treeLocDef = treeLocDefinition;
  sanitationForm: FormGroup;
  readonly sanitationDef = sanitationViolationsDefinition;
  sanitationCodeViolationsList: SanitationCodeViolations[] = [];
  rodentForm: FormGroup;
  readonly rodentDef = rodentBaitingDefinition;
  graffitiForm: FormGroup;
  readonly graffitiDef = graffitiDefinition;
  surfacesWithGraffiti: SurfacesWithGraffiti[] = [];
  structuresWithGraffiti: StructuresWithGraffiti[] = [];
  vehicleForm: FormGroup;
  readonly vehicleDef = abandonedVehicleDefinition;
  vehicleMakeModels: VehicleMakeModel[] = [];
  vehicleColors: VehicleColor[] = [];

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private elementRef: ElementRef,
              @Inject(DOCUMENT) private document,
              private resService: ResourcesService,
              private datePipe: DatePipe) {
  }

  public ngOnInit() {
    this.newRequest = !(this.route.snapshot.paramMap.has('requestId'));
    if (this.route.snapshot.paramMap.has('requestType')) {
      this.requestType = this.route.snapshot.paramMap.get('requestType');
    }
    if (this.newRequest) {
      this.initializeForms();
    } else {
      this.requestId = this.route.snapshot.paramMap.get('requestId');
      this.getRequest();
    }

  }

  initializeForms() {
    console.log('requestType is', this.requestType);
    this.requestForm = this.fb.group(this.requestBaseDef);
    if (this.currentRequest) {
      Object.keys(this.requestForm.value).forEach(
        key => this.requestForm.patchValue( {[key]: this.currentRequest[key]})
      );
      if (this.currentRequest.creationDate) {
        this.requestForm.get('creationDate')
          .setValue(this.datePipe.transform(this.currentRequest.creationDate, 'yyyy-MM-dd'));
      }
      if (this.currentRequest.completionDate) {
        this.requestForm.get('completionDate')
          .setValue(this.datePipe.transform(this.currentRequest.completionDate, 'yyyy-MM-dd'));
      }
    }
    this.getStreetNamesAndZipCodes();
    if (this.newRequest) {
      this.requestForm.get('status').setValue('Open');
      this.requestForm.get('status').disable();
    }
    if ((this.requestType === 'abandoned_vehicle') ||
        (this.requestType === 'garbage_cart') ||
        (this.requestType === 'graffiti') ||
        (this.requestType === 'pothole')) {

      this.ssaForm = this.fb.group(this.ssaDef);
      if (this.currentRequest && this.currentRequest.ssa) {
        Object.keys(this.ssaForm.value).forEach(
          key => this.ssaForm.patchValue({ [key]: this.currentRequest.ssa[key] })
        );
        this.ssaForm.updateValueAndValidity();
      }
    }
    if ((this.requestType === 'abandoned_vehicle') ||
        (this.requestType === 'garbage_cart') ||
        (this.requestType === 'pothole') ||
        (this.requestType === 'rodents') ||
        (this.requestType === 'tree_debris')) {
      this.actionsForm = this.fb.group(this.actionsDef);
      this.getActivitiesAndActions();
    }
    if ((this.requestType === 'abandoned_vehicle')) {
      this.vehicleForm = this.fb.group(this.vehicleDef);
      this.getVehicleModelsAndColors();
    }
    if (this.requestType === 'garbage_cart') {
      this.garbageForm = this.fb.group(this.garbageDef);
      if (this.currentRequest && this.currentRequest.garbageCartsDelivered) {
        Object.keys(this.garbageForm.value).forEach(
          key => this.garbageForm.patchValue({ [key]: this.currentRequest.garbageCartsDelivered[key] })
        );
        this.garbageForm.updateValueAndValidity();
      }
    }
    if (this.requestType === 'graffiti') {
      this.graffitiForm = this.fb.group(this.graffitiDef);
      this.getGraffitiSurfacesAndStructures();
    }
    if (this.requestType === 'pothole') {
      this.potholesForm = this.fb.group(this.potholesdDef);
      if (this.currentRequest && this.currentRequest.potholesFilled) {
        Object.keys(this.potholesForm.value).forEach(
          key => this.potholesForm.patchValue({ [key]: this.currentRequest.potholesFilled[key] })
        );
        this.potholesForm.updateValueAndValidity();
      }
    }
    if (this.requestType === 'rodents') {
      this.rodentForm = this.fb.group(this.rodentDef);
      if (this.currentRequest && this.currentRequest.rodentBaitingDetails) {
        Object.keys(this.rodentForm.value).forEach(
          key => this.rodentForm.patchValue({ [key]: this.currentRequest.rodentBaitingDetails[key] })
        );
        this.rodentForm.updateValueAndValidity();
      }
    }
    if (this.requestType === 'sanitation') {
      this.sanitationForm = this.fb.group(this.sanitationDef);
      this.getSanitationViolations();
    }
    if ((this.requestType === 'tree_debris') || (this.requestType === 'tree_trim')) {
      this.treeLocForm = this.fb.group(this.treeLocDef);
      if (this.currentRequest && this.currentRequest.locationOfTreeInRequest) {
        Object.keys(this.treeLocForm.value).forEach(
          key => this.treeLocForm.patchValue({ [key]: this.currentRequest.locationOfTreeInRequest[key] })
        );
        this.treeLocForm.updateValueAndValidity();
      }
    }
  }

  checkIfFormIsEmpty(formName: string, formDefinition: string) {
    return !Object.keys(this[formDefinition]).some(
      key => ((this[formName].get(key).value !== '') &&
        (this[formName].get(key).value !== [])));
  }

  submitForm() {
    if (this.requestForm.valid) {
      console.log('form is valid');
      let submittedRequest: Request;
      if (this.currentRequest) {
        submittedRequest = this.currentRequest;
      } else {
        submittedRequest = new Request();
      }
      Object.keys(this.requestForm.value).forEach(
        key => submittedRequest[key] = this.requestForm.get(key).value
      );
      submittedRequest.streetName = {streetName: this.requestForm.get('streetName').value};
      submittedRequest.zipCode = {zipCode: +this.requestForm.get('zipCode').value};
      submittedRequest.latitude = (this.currentRequest && this.currentRequest.latitude) ?
                                  this.currentRequest.latitude : +document.getElementById('latVal').innerText;
      submittedRequest.longitude = (this.currentRequest && this.currentRequest.longitude) ?
                                   this.currentRequest.longitude : +document.getElementById('lonVal').innerText;
      submittedRequest.xCoordinate = (this.currentRequest && this.currentRequest.xCoordinate) ?
                                     this.currentRequest.xCoordinate : +document.getElementById('coordsX').innerText;
      submittedRequest.yCoordinate = (this.currentRequest && this.currentRequest.yCoordinate) ?
                                     this.currentRequest.yCoordinate : +document.getElementById('coordsY').innerText;

      if (this.ssaForm && !this.checkIfFormIsEmpty('ssaForm', 'ssaDef')) {
        // add ssa to request
        if (!submittedRequest.ssa) {
          submittedRequest.ssa = new SsaInRequest();
        }
        Object.keys(this.ssaForm.value).forEach(
          key => submittedRequest.ssa[key] = this.ssaForm.get(key).value
        );
      }

      if (this.actionsForm && !this.checkIfFormIsEmpty('actionsForm', 'actionsDef')) {
        // add actions
        if (!submittedRequest.recentActionsOnRequest) {
          submittedRequest.recentActionsOnRequest = new RecentActionsOnRequest();
        }
        Object.keys(this.actionsForm.value).forEach(
          key => submittedRequest.recentActionsOnRequest[key] = this.actionsForm.get(key).value
        );
      }

      if (this.garbageForm && !this.checkIfFormIsEmpty('garbageForm', 'garbageDef')) {
        //  add garbageForm
        if (!submittedRequest.garbageCartsDelivered) {
          submittedRequest.garbageCartsDelivered = new GarbageCartsDelivered();
        }
        Object.keys(this.garbageForm.value).forEach(
          key => submittedRequest.garbageCartsDelivered[key] = this.garbageForm.get(key).value
        );
      }

      if (this.potholesForm && !this.checkIfFormIsEmpty('potholesForm', 'potholesdDef')) {
        // add potholes
        if (!submittedRequest.potholesFilled) {
          submittedRequest.potholesFilled = new PotholesFilled();
        }
        Object.keys(this.potholesForm.value).forEach(
          key => submittedRequest.potholesFilled[key] = this.potholesForm.get(key).value
        );
      }

      if (this.treeLocForm && !this.checkIfFormIsEmpty('treeLocForm', 'treeLocDef')) {
        // add treeLocs
        if (!submittedRequest.locationOfTreeInRequest) {
          submittedRequest.locationOfTreeInRequest = new LocationOfTreeInRequest();
        }
        Object.keys(this.treeLocForm.value).forEach(
          key => submittedRequest.locationOfTreeInRequest[key] = this.treeLocForm.get(key).value
        );
      }

      if (this.sanitationForm && !this.checkIfFormIsEmpty('sanitationForm', 'sanitationDef')) {
        // add sanitation
        if (!submittedRequest.sanitationViolationsInRequest) {
          submittedRequest.sanitationViolationsInRequest = new SanitationViolationsInRequest();
        }
        Object.keys(this.sanitationForm.value).forEach(
          key => submittedRequest.sanitationViolationsInRequest[key] = this.sanitationForm.get(key).value
        );
      }

      if (this.rodentForm && !this.checkIfFormIsEmpty('rodentForm', 'rodentDef')) {
        // add rodents
        if (!submittedRequest.rodentBaitingDetails) {
          submittedRequest.rodentBaitingDetails = new RodentBaitingDetails();
        }
        Object.keys(this.rodentForm.value).forEach(
          key => submittedRequest.rodentBaitingDetails[key] = this.rodentForm.get(key).value
        );
      }

      if (this.graffitiForm && !this.checkIfFormIsEmpty('graffitiForm', 'graffitiDef')) {
        // add graffiti
        if (!submittedRequest.graffitiDetails) {
          submittedRequest.graffitiDetails = new GraffitiDetails();
        }
        Object.keys(this.graffitiForm.value).forEach(
          key => submittedRequest.graffitiDetails[key] = this.graffitiForm.get(key).value
        );
      }

      if (this.vehicleForm && !this.checkIfFormIsEmpty('vehicleForm', 'vehicleDef')) {
        // add abandoned vehicle
        if (!submittedRequest.abandonedVehicleDetails) {
          submittedRequest.abandonedVehicleDetails = new AbandonedVehicleDetails();
        }
        Object.keys(this.vehicleForm.value).forEach(
          key => submittedRequest.abandonedVehicleDetails[key] = this.vehicleForm.get(key).value
        );
      }

      this.successMessage = '';
      if (this.newRequest) {
        const d = new Date();
        submittedRequest.creationDate = this.datePipe.transform(d, 'yyyy-MM-dd');
        submittedRequest.requestType = {type: this.requestTypes[this.requestType]};
        let srID = `${this.datePipe.transform(d, 'yyyyMMdd')}`;
        srID = `${srID}-${Object.keys(this.requestTypes).indexOf(this.requestType)}-`;
        srID = `${srID}${this.datePipe.transform(d, 'hhmmss')}`;
        submittedRequest.serviceRequestNumber = srID;

        console.log(JSON.stringify(submittedRequest));
        this.resService.addRequest(submittedRequest).subscribe(
          res => console.log('addRequest responded:', JSON.stringify(res)),
          er => console.log(er),
          () => {
            this.successMessage = 'The changes were saved successfully';
            window.scrollTo(1, 300);
          }
        );
      } else {
        if (!submittedRequest.completionDate && this.requestForm.get('status').value.includes('Completed')) {
          const d = new Date();
          submittedRequest.completionDate = this.datePipe.transform(d, 'yyyy-MM-dd');
        }

        console.log(JSON.stringify(submittedRequest));
        this.resService.updateRequest(submittedRequest).subscribe (
          res => console.log('updateRequest responded:', JSON.stringify(res)),
          er => console.log(er),
          () => {
            this.successMessage = 'The changes were saved successfully';
            window.scrollTo(1, 300);
          }
        );
      }
    }
  }

  getRequest() {
    this.resService.getRequestById(this.requestId).subscribe (
      req => this.currentRequest = req,
      er => console.log(er),
      () => {
        if (this.currentRequest) {
          console.log(JSON.stringify(this.currentRequest, null, 2));
          this.requestType = Object.keys(this.requestTypes)
            .filter(x => this.requestTypes[x] === this.currentRequest.requestType.type)[0];
          this.initializeForms();
        }
      }
    );

  }

  getStreetNamesAndZipCodes() {
    this.resService.getAllStreetNames().pipe(
      tap(st => this.streetNames = st),
      mergeMap( res => this.resService.getAllZipCodes())
    ).subscribe(
      res => this.zipCodes = res,
      er => console.log(er),
      () => {
        if (this.currentRequest && this.requestForm) {
          this.requestForm.patchValue({streetName: this.currentRequest.streetName.streetName});
          this.requestForm.patchValue({zipCode: this.currentRequest.zipCode.zipCode});
        }
      }
    );
  }

  getActivitiesAndActions() {
    this.resService.getActivitiesByType(this.requestTypes[this.requestType]).pipe(
      tap(acts => this.currentActivitiesList = acts ),
      mergeMap(acts => this.resService.getActionsByType(this.requestTypes[this.requestType]))).subscribe(
      acts => this.mostRecentActionsList = acts,
      er => console.log(er),
      () => {
        if (this.currentRequest && this.currentRequest.recentActionsOnRequest) {
          Object.keys(this.actionsForm.value).forEach(
            key => this.actionsForm.patchValue({ [key]: this.currentRequest.recentActionsOnRequest[key] })
          );
          this.actionsForm.updateValueAndValidity();
        }
      }
    );
  }

  getVehicleModelsAndColors() {
    this.resService.getAllVehicleMakeModels().pipe(
      tap(mm => this.vehicleMakeModels = mm),
      mergeMap(mm => this.resService.getAllVehicleColors())).subscribe(
      vc => this.vehicleColors = vc,
      er => console.log(er),
      () => {
        if (this.currentRequest && this.currentRequest.abandonedVehicleDetails) {
          Object.keys(this.vehicleForm.value).forEach(
            key => this.vehicleForm.patchValue({ [key]: this.currentRequest.abandonedVehicleDetails[key] })
          );
          this.vehicleForm.updateValueAndValidity();
        }

      }
    );
  }

  getGraffitiSurfacesAndStructures() {
    this.resService.getAllGraffitiSurfaces().pipe(
      tap(gsur => this.surfacesWithGraffiti = gsur),
      mergeMap(gsur => this.resService.getAllGraffitiStructures())).subscribe (
      res => this.structuresWithGraffiti = res,
      er => console.log(er),
      () => {
        if (this.currentRequest && this.currentRequest.graffitiDetails) {
          Object.keys(this.graffitiForm.value).forEach(
            key => this.graffitiForm.patchValue({ [key]: this.currentRequest.graffitiDetails[key] })
          );
          this.graffitiForm.updateValueAndValidity();
        }
      }
    );
  }

  getSanitationViolations() {
    this.resService.getAllSanitationViolations().subscribe(
      res => this.sanitationCodeViolationsList = res,
      er => console.log(er),
      () => {
        if (this.currentRequest && this.currentRequest.sanitationViolationsInRequest) {
          Object.keys(this.sanitationForm.value).forEach(
            key => this.sanitationForm.patchValue({ [key]: this.currentRequest.sanitationViolationsInRequest[key] })
          );
          this.sanitationForm.updateValueAndValidity();
        }

      }
    );
  }


  ngAfterViewInit() {
    const s = this.document.createElement('script');
    s.type = `text/javascript`;
    s.src = '//cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js';
    // s.src = '//openlayers.org/en/v4.6.5/build/ol.js';
    const __this = this;
    s.onload = function () { __this.afterScriptAdded(); };
    this.elementRef.nativeElement.appendChild(s);
  }

  afterScriptAdded() {
    const __this = this;
    this.map = new ol.Map({
      target: 'map',
      controls: ol.control.defaults({
        attributionOptions: {
          collapsible: false
        }
      }),
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([this.lon, this.lat]),
        zoom: 12
      })
    });
    const marker = new ol.Overlay({
      position: ol.proj.fromLonLat([__this.lon, __this.lat]),
      positioning: 'center-center',
      element: document.getElementById('marker')
    });
    this.map.addOverlay(marker);

    this.map.on('click', function (args) {
      console.log(args.coordinate);
      const lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      console.log(lonlat);

      const lon = lonlat[0];
      const lat = lonlat[1];
      // alert(`lat: ${lat} long: ${lon}`);

      /*const m = __this.document.createElement('div');
      m.id = 'marker';
      __this.elementRef.nativeElement.appendChild(m);*/
      const markerPopup = new ol.Overlay({
        position: ol.proj.fromLonLat([lon, lat]),
        positioning: 'center-center',
        element: document.getElementById('marker')
      });
      this.addOverlay(markerPopup);


      document.getElementById('coordsX').innerText = args.coordinate[0];
      document.getElementById('coordsY').innerText = args.coordinate[1];
      document.getElementById('latVal').innerText = lonlat[0];
      document.getElementById('lonVal').innerText = lonlat[1];
    });
  }


}
