import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { ResourcesService } from '../services/resources.service';
import { StreetNames, User } from '../domain/typeScriptClasses';
import { getCookie } from '../utils/cookie-funs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent implements OnInit {
  errorMessage: string;

  usernamesList: string[] = [];

  isRegistered: boolean;

  userForm: FormGroup;
  readonly signinDef = {
    username: ['', Validators.required],
    password: ['', Validators.required]
  };
  readonly registrationDef = {
    username: ['', Validators.required],
    password: ['', Validators.required],
    confirm_password: ['', Validators.required],
    firstname: [''],
    lastname: [''],
    email: ['', [Validators.required, Validators.email]],
    street: [''],
    streetno: ['']
  };

  streetNames: StreetNames[] = [];

  constructor(private fb: FormBuilder,
              private authService: AuthenticationService,
              private resService: ResourcesService) { }

  ngOnInit() {
    this.isRegistered = true;
    this.userForm = this.fb.group(this.signinDef);
  }

  showRegistration() {
    this.isRegistered = false;
    this.userForm = this.fb.group(this.registrationDef, {validator: checkPasswords});
    if (this.streetNames.length === 0) {
      this.getStreetNames();
    }
    if (this.usernamesList.length === 0) {
      this.getUsernames();
    }
  }

  showSignIn() {
    this.isRegistered = true;
    this.userForm = this.fb.group(this.signinDef);
  }

  submitForm() {
    if (this.userForm.valid) {
      console.log(JSON.stringify(this.userForm.value));
      if (this.isRegistered) {
        this.signIn();
      } else {
        this.register();
      }
    }
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  register() {
    this.errorMessage = '';
    if (!this.usernamesList.some( x => x === this.userForm.get('username').value)) {
      const newUser = new User();
      newUser.username = this.userForm.get('username').value;
      newUser.password = this.userForm.get('password').value;
      newUser.email = this.userForm.get('email').value;
      newUser.firstname = this.userForm.get('firstname').value;
      newUser.lastname = this.userForm.get('lastname').value;
      newUser.street = this.userForm.get('street').value;
      newUser.streetno = this.userForm.get('streetno').value;
      this.authService.register(newUser).subscribe(
        us => console.log(JSON.stringify(us)),
        er => {
          console.log(er);
          sessionStorage.clear();
        },
        () => window.location.href = '/home'
      );

    } else {

      if (this.usernamesList.length > 0) {
        this.errorMessage = 'This username already exists!';
        window.scrollTo(1, 1);
      }
    }
  }

  signIn() {
    this.authService.login(this.userForm.get('username').value,
                           this.userForm.get('password').value);
  }

  getUsernames() {
    this.authService.getAllUsernames().subscribe(
      usrs => this.usernamesList = usrs,
      er => console.log(er)
    );
  }

  getStreetNames() {
    this.resService.getAllStreetNames().subscribe(
      res => this.streetNames = res,
      er => console.log(er)
    );
  }

}

export function checkPasswords(group: FormGroup) {
  return group.get('password').value === group.get('confirm_password').value ? null : {res: true};
}
