import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StreetNames, ZipCodes, Request } from '../domain/typeScriptClasses';
import { ResourcesService } from '../services/resources.service';
import { mergeMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html'
})
export class RequestsListComponent implements OnInit {
  results: Request[] = [];
  resultsTitle = 'Results';
  streetNames: StreetNames[] = [];
  zipCodes: ZipCodes[] = [];
  searchForm: FormGroup;
  readonly formDefinition = { strName: [''],
                              zipCode: [''] };

  inStreetNameSearch: boolean;
  currentPage = 0;
  totalPages = 0;
  itemsPerPage = 10;

  constructor(private fb: FormBuilder,
              private resService: ResourcesService) { }

  ngOnInit() {
    this.getLists();
  }

  getLists() {
    this.resService.getAllStreetNames().pipe(
      tap(st => this.streetNames = st),
      mergeMap( res => this.resService.getAllZipCodes())
    ).subscribe(
      res => this.zipCodes = res,
      er => console.log(er),
      () => this.searchForm = this.fb.group(this.formDefinition)
    );
  }

  getRequestsInStreet(setCurrentPageToZero: boolean) {
    if ( this.searchForm && this.searchForm.valid &&
         (this.searchForm.get('strName').value !== '') ) {

      if (setCurrentPageToZero) {
        this.currentPage = 0;
      }
      this.inStreetNameSearch = true;
      this.results = [];
      const currentOffset = this.currentPage * this.itemsPerPage;
      this.resService.getRequestsInStreet(this.searchForm.get('strName').value, currentOffset, this.itemsPerPage)
        .subscribe(res => {
            this.results = res.resultSet;
            this.totalPages = Math.ceil(res.total / this.itemsPerPage);
                      this.resultsTitle = 'Incidents in ' + this.searchForm.get('strName').value;
                    },
                   er => console.log(er)
      );
    }
  }

  getRequestsInZipcode(setCurrentPageToZero: boolean) {
    if (this.searchForm && this.searchForm.valid &&
        (this.searchForm.get('zipCode').value !== '')) {

      if (setCurrentPageToZero) {
        this.currentPage = 0;
      }
      this.inStreetNameSearch = false;
      this.results = [];
      const currentOffset = this.currentPage * this.itemsPerPage;
      this.resService.getRequestsInZipcode(this.searchForm.get('zipCode').value, currentOffset, this.itemsPerPage)
        .subscribe(res => {
                          this.results = res.resultSet;
                          this.totalPages = Math.ceil(res.total / this.itemsPerPage);
                          this.resultsTitle = 'Incidents in ' + this.searchForm.get('zipCode').value;
                        },
                        er => console.log(er)
        );
    }
  }

  changeItemsPerPage(event: any) {
    this.itemsPerPage = +event.target.value;
    if (this.inStreetNameSearch) {
      this.getRequestsInStreet(true);
    } else {
      this.getRequestsInZipcode(true);
    }
  }

  goToNextPage() {
    if ((this.currentPage + 1) < this.totalPages) {
      this.currentPage++;
      if (this.inStreetNameSearch) {
        this.getRequestsInStreet(false);
      } else {
        this.getRequestsInZipcode(false);
      }
    }
  }

  goToPreviousPage() {
    if ((this.currentPage - 1) >= 0) {
      this.currentPage --;
      if (this.inStreetNameSearch) {
        this.getRequestsInStreet(false);
      } else {
        this.getRequestsInZipcode(false);
      }
    }
  }


}
