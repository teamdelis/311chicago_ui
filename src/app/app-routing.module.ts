import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { RequestsListComponent } from './requests-list/requests-list.component';
import { EditRequestComponent } from './edit-request/edit-request.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RequestsStatsComponent } from './requesta-stats/requests-stats.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'requests',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: '/find',
        pathMatch: 'full'
      },
      {
        path: 'find',
        component: RequestsListComponent
      },
      {
        path: 'statistics',
        component: RequestsStatsComponent
      },
      {
        path: 'new/:requestType',
        component: EditRequestComponent
      },
      {
        path: 'edit/:requestId',
        component: EditRequestComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
