export const environment = {
  production: true,
  API_ENDPOINT: '/311chicago-service',
  LOGOUT_URL: '/user/logout'
};
